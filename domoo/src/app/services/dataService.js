'use strict';

angular.module('domoo')
  .service('dataService', ['$firebaseObject', function($firebaseObject){
    var self = this;
    
    var dataReference = new Firebase("https://burning-fire-8878.firebaseio.com/data");
    self.data = $firebaseObject(dataReference);
  }]);
  