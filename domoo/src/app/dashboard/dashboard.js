 'use strict';

angular.module('domoo')
  .controller('DashboardCtrl', ['$scope', '$interval', '$firebaseObject', '$firebaseArray', function($scope, $interval, $firebaseObject, $firebaseArray){
    var self = this;
    
    //initialize plant value to untouched, so user is asket to touch his plant on load
    
    // loader
    self.isLoaded = false;
    self.mode = "query";
    $interval(function() {
      self.mode = (self.mode == 'query' ? 'determinate' : 'query');
    }, 0, 0, true);
    
    var ref = new Firebase("https://burning-fire-8878.firebaseio.com/data");
    self.obj = $firebaseObject(ref);
    
    self.obj.$loaded().then(function(data){
      if(self.isLoaded === false){
        self.isLoaded = true; 
      }
      data.$bindTo($scope, 'data');

    })
      .catch(function(error){
      console.log('Purtroppo, :' + error);
    });
    
    
}]);