'use strict';

angular.module('domoo')
  .controller('GraphDayDataCtrl', ['$scope', 'mainService', '$timeout', '$interval', '$location', '$firebaseObject', '$firebaseArray', function($scope, mainService, $timeout, $interval, $location, $firebaseObject, $firebaseArray){
    var self = this;
    
    self.selectedSensor = 1;
    // loader
    self.isLoaded = false;
    self.mode = "query";
    $interval(function() {
      self.mode = (self.mode == 'query' ? 'determinate' : 'query');
    }, 0, 0, true);

    var dailyData = new Firebase("https://burning-fire-8878.firebaseio.com/day");
    var dayQuery = dailyData.limitToLast(7);
    
    $scope.dayData = $firebaseArray(dayQuery);
    
    self.colours = [{
      fillColor: 'rgba(128, 203, 196, 0.5)',
      strokeColor: 'rgb(178, 223, 219)',
      highlightFill: 'rgba(128, 203, 196, 0.5)',
      highlightStroke: 'rgb(178, 223, 219)',
    }];
    
    $scope.$watch('dayData', function(newVal) {
      $scope.dayData.$loaded().then(function(data){
        createSensorGraph(data);
        self.setGraphValues();
      }); 
    }, true);
    
    self.setGraphValues = function(){
      if(self.selectedSensor == 1){
        self.graphData = self.tempGraphDataContainer;
        self.graphLabels = self.tempLabels;
        self.mainLabel = "temperature";
      } else if(self.selectedSensor == 2){
        self.graphData = self.airHumidityGraphDataContainer;
        self.graphLabels = self.airHumidityLabels;
        self.mainLabel = "air humidity";
      } else if(self.selectedSensor == 3){
        self.graphData = self.illuminationGraphDataContainer;
        self.graphLabels = self.illuminationLabels;
        self.mainLabel = "sunlight";
      } else if(self.selectedSensor == 4){
        self.graphData = self.soilHumidityGraphDataContainer;
        self.graphLabels = self.soilHumidityLabels;
        self.mainLabel = "soil humidity";
      }
    }
    
    function createSensorGraph(data){
      buildSensorGraph(data, 'temperature');
      buildSensorGraph(data, 'airHumidity');
      buildSensorGraph(data, 'illumination');
      buildSensorGraph(data, 'soilHumidity');
      buildSensorGraph(data, 'potentiometer');
    }
    
    function buildSensorGraph(data, sensor){
      var graphDataContainer = [];
      var graphData = [];
      var graphLabels = [];
      data.forEach(function(elem){
        graphData.push(elem[sensor]);
        graphLabels.push(elem.day);
      });
      graphDataContainer.push(graphData);
      if(sensor == 'temperature'){
        self.tempGraphDataContainer = graphDataContainer;
        self.tempLabels = graphLabels;
      } else if(sensor == 'airHumidity'){
        self.airHumidityGraphDataContainer = graphDataContainer;
        self.airHumidityLabels = graphLabels;
      }else if(sensor == 'illumination'){
        self.illuminationGraphDataContainer = graphDataContainer;
        self.illuminationLabels = graphLabels;
      }else if(sensor == 'soilHumidity'){
        self.soilHumidityGraphDataContainer = graphDataContainer;
        self.soilHumidityLabels = graphLabels;
      }
    }
  }]);