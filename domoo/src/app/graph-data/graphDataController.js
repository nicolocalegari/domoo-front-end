'use strict';

angular.module('domoo')
  .controller('GraphDataCtrl', ['$scope', 'mainService', '$timeout', '$interval', '$location', '$firebaseObject', '$firebaseArray', function($scope, mainService, $timeout, $interval, $location, $firebaseObject, $firebaseArray){
    var self = this;

    // loader
    self.isLoaded = false;
    self.mode = "query";
    $interval(function() {
      self.mode = (self.mode == 'query' ? 'determinate' : 'query');
    }, 0, 0, true);

    var hourlyData = new Firebase("https://burning-fire-8878.firebaseio.com/hour");
    var hourQuery = hourlyData.limitToLast(24);
    $scope.hourData = $firebaseArray(hourQuery);
    
    //initialize values for graph
          
    self.colours = [{
      fillColor: 'rgba(128, 203, 196, 0.5)',
      strokeColor: 'rgb(178, 223, 219)',
      highlightFill: 'rgba(128, 203, 196, 0.5)',
      highlightStroke: 'rgb(178, 223, 219)',
    }];
    
    $scope.hourData.$loaded().then(function(data){
      self.selectedSensor = 1;
      createSensorGraph(data);
      self.setGraphValues();
    });

    $scope.$watch('hourData', function(newVal) {
      $scope.hourData.$loaded().then(function(data){
        createSensorGraph(data);
        self.setGraphValues();
      }); 
    }, true);

    self.setGraphValues = function(){
      if(self.selectedSensor == 1){
        self.graphData = self.tempGraphDataContainer;
        self.graphLabels = self.tempLabels;
        self.mainLabel = "temperature";
      } else if(self.selectedSensor == 2){
        self.graphData = self.airHumidityGraphDataContainer;
        self.graphLabels = self.airHumidityLabels;
        self.mainLabel = "air humidity";
      } else if(self.selectedSensor == 3){
        self.graphData = self.illuminationGraphDataContainer;
        self.graphLabels = self.illuminationLabels;
        self.mainLabel = "sunlight";
      } else if(self.selectedSensor == 4){
        self.graphData = self.soilHumidityGraphDataContainer;
        self.graphLabels = self.soilHumidityLabels;
        self.mainLabel = "soil humidity";
      }
    }

    function createSensorGraph(data){
      buildSensorGraph(data, 'temperature');
      buildSensorGraph(data, 'airHumidity');
      buildSensorGraph(data, 'illumination');
      buildSensorGraph(data, 'soilHumidity');
      buildSensorGraph(data, 'potentiometer');
    }

    function buildSensorGraph(data, sensor){
      var graphDataContainer = [];
      var graphData = [];
      var graphLabels = [];
      data.forEach(function(elem){
        graphData.push(elem[sensor]);
        graphLabels.push(elem.hour);
      });
      graphDataContainer.push(graphData);
      if(sensor == 'temperature'){
        self.tempGraphDataContainer = graphDataContainer;
        self.tempLabels = graphLabels;
      } else if(sensor == 'airHumidity'){
        self.airHumidityGraphDataContainer = graphDataContainer;
        self.airHumidityLabels = graphLabels;
      }else if(sensor == 'illumination'){
        self.illuminationGraphDataContainer = graphDataContainer;
        self.illuminationLabels = graphLabels;
      }else if(sensor == 'soilHumidity'){
        self.soilHumidityGraphDataContainer = graphDataContainer;
        self.soilHumidityLabels = graphLabels;
      }
    }

    /*
    function pushData(arrayIn, graphArray){
      var dataArray = [];
      arrayIn.forEach(function(elem){
        if(self.selectedSensor == 1)
        dataArray.push(parseFloat(elem.temperature));
        else if(self.selectedSensor == 2)
        dataArray.push(parseFloat(elem.airHumidity));
        else if(self.selectedSensor == 3)
        dataArray.push(parseFloat(elem.illumination));
        else if(self.selectedSensor == 4)
        dataArray.push(parseFloat(elem.soilHumidity));
      });
      graphArray.push(dataArray);
      return graphArray;
    }


    function pushTime(arrayIn, graphArray){
      arrayIn.forEach(function(elem){
        graphArray.push((elem.time));
        return graphArray;
      })
    }

    function pushDay(arrayIn, graphArray){
      arrayIn.forEach(function(elem){
        graphArray.push((elem.day));
        console.log(elem.day);
        return graphArray;
      })
    }

    self.refreshHourGraph = function(){
      hourData.$loaded().then(function(data){
        if(self.isLoaded === false){
          self.isLoaded = true; 
        }
        self.hourGraphData = [];
        self.hourGraphLabels = [];
        pushData(data, self.hourGraphData);
        pushTime(data, self.hourGraphLabels);
        //console.log('the data to build the daily graph are here: ' + self.hourGraphData +  self.hourGraphLabels);
      });
    }

    $scope.$watch(self.hourGraphData, function(newVal){
      console.log('graph changed');
      self.refreshHourGraph();
    }, true);

    self.refreshDayGraph = function(){
      dayData.$loaded().then(function(data){
      self.dayGraphData = [];
      self.dayGraphLabels = [];
      //console.log(data);
      pushData(data, self.dayGraphData);
      pushDay(data, self.dayGraphLabels);
      //console.log('the data to build the daily graph are here: ' + self.dayGraphData +  self.dayGraphLabels);
      });
    }
    */

  }]);