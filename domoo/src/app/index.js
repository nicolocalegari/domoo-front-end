'use strict';

angular.module('domoo', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngResource', 'ui.router', 'ui.router.stateHelper', 'ngMaterial', 'firebase', 'chart.js', 'ui.materialize'])
  .config(function ($urlRouterProvider, stateHelperProvider) {
  stateHelperProvider
    .state({
    name: 'dashboard',
    templateUrl: 'app/dashboard/dashboard.html',
    controller: 'DashboardCtrl as ctrl',
    abstract: true,
    children : [
      {
       name: 'values',
       url: '/',
       views: {
         'sensors': {
           templateUrl: 'app/sensors/sensors.html',
           controller: 'SensorsCtrl as ctrl'
         },
         'graphData': {
           templateUrl: 'app/graph-data/graph-data.html',
           controller: 'GraphDataCtrl as ctrl'
         }, 
         'graphDayData': {
           templateUrl: 'app/graph-day/graph-day.html',
           controller: 'GraphDayDataCtrl as ctrl'
         },
         'forecast': {
           templateUrl: 'app/weather-forecast/forecast.html',
           controller: 'ForecastCtrl as ctrl'
         },
       }
      }
    ]
  })
  .state({
      name: 'settings',
      templateUrl: 'app/settings/settings.html',
      url:'/settings',
      controller: 'SettingsCtrl as ctrl'
    })
  .state({
      name: 'filter',
      templateUrl: 'app/filter/filter.html',
      url:'/filter',
      controller: 'FilterCtrl as ctrl'
    })
})

  .config(function($mdThemingProvider) {
  $mdThemingProvider.theme('default')
    .primaryPalette('blue-grey', {
    'default': '400', // by default use shade 400 from the teal palette for primary intentions
    'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
    'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
    'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
  })
  // If you specify less than all of the keys, it will inherit from the
  // default shades
    .accentPalette('teal', {
    'default': '200' // use shade 200 for default, and keep all other shades the same
  });
})