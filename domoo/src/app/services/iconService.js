'use strict';
angular.module('domoo')
  .service('iconService', function(){
    var self = this;
    
    var path = 'assets/icons/';
    self.icons = {
      sunny : path + 'sunny.svg',
      partiallySunny : path + 'partially-sunny.svg',
      cloud : path +'cloud.svg',
      rainy : path + 'rain.svg'
    };
  });