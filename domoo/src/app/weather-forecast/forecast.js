'use strict';
angular.module('domoo')
  .controller('ForecastCtrl', ['$scope', 'settingsService', 'iconService', '$interval', function($scope, settingsService, iconService, $interval){
    var self = this;

    settingsService.settings.$loaded().then(function(data){
     self.selectedCity = settingsService.selectedCity;
      var p = settingsService.requestData();
      res(p);
    });

    function res(promise){
      promise.success(function(data){   
        var forecastInfo = data.list;
        var today = forecastInfo[0];
        var tomorrow = forecastInfo[1];
        self.todayAverageTemp = today.temp.day;
        self.todayMinTemp = today.temp.min;
        self.todayMaxTemp = today.temp.max;
        self.todayHumidity = today.humidity;
        self.todayPressure = parseInt(today.pressure);
        self.todayWind = today.speed;
        self.todayIcon = weatherIcon(today);

        self.tomorrowAverageTemp = tomorrow.temp.day;
        self.tomorrowMinTemp = tomorrow.temp.min;
        self.tomorrowMaxTemp = tomorrow.temp.max;
        self.tomorrowHumidity = tomorrow.humidity;
        self.tomorrowPressure = parseInt(tomorrow.pressure);
        self.tomorrowWind = tomorrow.speed;
        self.tomorrowIcon = weatherIcon(tomorrow);
      });
    }

    function weatherIcon(day){
      if( day.weather[0].id > 799 && day.weather[0].id <= 801 ){
        return iconService.icons.sunny;
        console.log(self.icon);
      } else if(day.weather[0].id < 804 && day.weather[0].id > 801){
        return iconService.icons.cloudy;
      } else if(day.weather[0].id >= 500 && day.weather[0].id <= 531){
        return iconService.icons.rainy;
      } else if(day.weather[0].id == 804){
        return iconService.icons.cloudy;
      }
    }


  }]);