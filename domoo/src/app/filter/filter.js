'use strict';

angular.module('domoo')
  .controller('FilterCtrl', ['$scope', function($scope){
    var self = this;
    self.asd = "Asd";
    
    var currentTime = new Date();
    self.currentTime = currentTime;
    self.currentTimeFrom = currentTime;
    self.currentTimeTo = currentTime;
    self.month = ['Januar', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    self.monthShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    self.weekdaysFull = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    self.weekdaysLetter = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
    self.disable = [false, 1, 7];
    self.today = 'Oggi';
    self.clear = 'Cancella';
    self.close = 'Ok';
    var days = 15;
    self.minDate = (new Date(self.currentTime.getTime() - ( 1000 * 60 * 60 *24 * days ))).toISOString();
    self.maxDate = (new Date(self.currentTime.getTime() + ( 1000 * 60 * 60 *24 * days ))).toISOString();
    self.onSet = function (data) {
      console.log('Ho settato come data ' + data);
    };


  }])