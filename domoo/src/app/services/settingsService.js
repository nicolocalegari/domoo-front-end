'use strict';
angular.module('domoo')
  .service('settingsService', ['$http', '$interval', '$firebaseObject', function($http, $interval, $firebaseObject){
    var self = this;

    //Firebase weather settings
    var ref = new Firebase("https://burning-fire-8878.firebaseio.com");
    self.settings = ($firebaseObject(ref.child('settings')));

    self.settings.$loaded().then(function(data){
      self.selectedCity = data.selectedCity;
      self.requestData = function(){
        var cityWeather = 'http://api.openweathermap.org/data/2.5/forecast/daily?q=' + self.selectedCity + '&mode=json&units=metric&cnt=7';
        console.log(cityWeather);
        if(self.selectedCity){
          console.log('city selected succesfully: ' + self.selectedCity);
          return $http.get(cityWeather);
        } else {
          console.log('no city selected!', self.selectedCity);
        }
      }
    });

  }]);