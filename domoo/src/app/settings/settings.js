'use strict';
angular.module('domoo')
  .controller('SettingsCtrl', ['$scope', 'settingsService', 'iconService', '$interval', '$firebaseObject', function($scope, settingsService, iconService, $interval, $firebaseObject){
    var self = this;

    settingsService.settings.$loaded().then(function(data){
      console.log(data);
      data.$bindTo($scope, 'settings');
    });
   
    self.updateSelectedCity = function(){
      settingsService.selectedCity = $scope.settings.selectedCity;
    };
    
    self.updatePlantieName = function(){
      settingsService.plantieName = $scope.settings.plantieName;
    };
    
    self.choices = ['Baobab', 'Sequoia', 'Fat plant', 'Weed'];
    
  }]);