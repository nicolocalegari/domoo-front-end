'use strict';
angular.module('domoo')
  .service('mainService', ['$rootScope', '$http', '$firebaseObject', '$interval', function($rootScope, $http, $firebaseObject, $interval){
    var self = this;
    
    self.confalonieriPublicIp = 'http://2.238.52.37';
    self.confalonieriLocalIp = 'http://192.168.1.131';
    self.raspberryIp = 'http://192.168.1.99';
    self.binascoLocalIp = 'http://192.168.1.4';
    self.binascoPubIp = 'http://130.25.253.94';
    self.fireBase = 'https://burning-fire-8878.firebaseio.com/.json';
    self.getData = function(){
      console.log('data requested to backend app');
      return $http({method : 'GET', url : self.confalonieriLocalIp + ':4665/sensors', dataType: 'jsonp'});
    };
    
    //retrieve data realtime from firebase '/data' endpoint
    /*
    self.ref = new Firebase("https://burning-fire-8878.firebaseio.com/data");
    self.obj = $firebaseObject(self.ref);
    
    
    //retrieve data from express.js endpoint on machine (raspi or computer)
    
    self.getTemperature = function(){
      console.log('temperature data requested');
      return $http({method : 'GET', url : self.fireBase, dataType: 'jsonp'});
    };
    
    self.getPotentiometer = function(){
      console.log('potentiometer data requested');
      return $http({method: 'GET', url: self.fireBase, dataType: 'jsonp'});
    };
    
    self.getIllumination = function(){
      console.log('illumination data requested');
      return $http({method: 'GET', url: self.fireBase, dataType: 'jsonp'});
    };
    
    self.getHumidity = function(){
      console.log('humidity data requested');
      return $http({method: 'GET', url: self.fireBase, dataType: 'jsonp'});
    }; 
    */
}]);